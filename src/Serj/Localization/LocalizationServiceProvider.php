<?php namespace Serj\Localization;

use Illuminate\Support\ServiceProvider;
use Route;
use Request;
use Redirect;
use Session;

class LocalizationServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        $this->package('serj/localization');
    }

    public function register()
    {
        $app = $this->app;
        Route::filter('LocalizationRedirectFilter', function () use ($app) {
            $currentLocale = $app['localization']->getCurrentLocale();
            $defaultLocale = $app['localization']->getDefault();
            $params        = explode('/', Request::path());
            if (count($params) > 0) {
                $localeCode        = $params[0];
                $locales           = $app['localization']->getSupportedLocales();
                $hideDefaultLocale = $app['localization']->hideDefaultLocaleInURL();
                $redirection       = false;
                if (!empty($locales[ $localeCode ])) {
                    if ($localeCode === $defaultLocale && $hideDefaultLocale) {
                        $redirection = $app['localization']->getNonLocalizedURL();
                    }
                } else {
                    if ($currentLocale !== $defaultLocale || !$hideDefaultLocale) {
                        // If the current url does not contain any locale
                        // The system redirect the user to the very same url "localized"
                        // we use the current locale to redirect him
                        $redirection = $app['localization']->getLocalizedURL();
                    }
                }
                if ($redirection) {
                    // Save any flashed data for redirect
                    Session::reflash();

                    return Redirect::to($redirection, 307)->header('Vary', 'Accept-Language');
                }
            }
        });
        Route::filter('LocalizationRoutes', function () {
            $app       = $this->app;
            $routeName = $app['localization']->getRouteNameFromAPath($app['router']->current()->uri());
            $app['localization']->setRouteName($routeName);

            return;
        });
        $app['config']->package('serj/localization', __DIR__.'/../config');
        $app['localization'] = $app->share(function () use ($app) {
            return new Localization($app['config'], $app['view'], $app['translator'], $app['router'], $app);
        });
    }

    public function provides()
    {
        return [];
    }
}
