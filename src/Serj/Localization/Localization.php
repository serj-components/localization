<?php namespace Serj\Localization;

use Illuminate\Config\Repository;
use Illuminate\View\Factory;
use Illuminate\Translation\Translator;
use Illuminate\Routing\Router;
use Illuminate\Routing\Route;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\URL;
use Session;
use Cookie;

class Localization
{
    protected $configRepository;
    protected $view;
    protected $translator;
    protected $router;
    protected $request;
    protected $app;
    protected $baseUrl;
    protected $defaultLocale;
    protected $supportedLocales;
    protected $currentLocale = false;
    protected $translatedRoutes = [];
    protected $routeName;

    public function __construct(
        Repository $configRepository,
        Factory $view,
        Translator $translator,
        Router $router,
        Application $app
    )
    {
        $this->configRepository = $configRepository;
        $this->view             = $view;
        $this->translator       = $translator;
        $this->router           = $router;
        $this->app              = $app;
        $this->request          = $this->app['request'];
        // set default locale
        $this->defaultLocale    = $this->getLocaleFromCode($this->configRepository->get('app.locale'));
        $this->supportedLocales = $this->getSupportedLocales();
    }

    public function setLocale($localeCode = null)
    {

        if (empty($localeCode) || !is_string($localeCode)) {
            $localeCode = $this->request->segment(1);
        }
        if ($this->isSupportedLocaleCode($localeCode)) {
            $this->currentLocale = $this->getLocaleFromCode($localeCode);
        } else {
            $localeCode          = null;
            $this->currentLocale = $this->getCurrentLocale();
        }
        $this->app->setLocale($this->getCurrentLocaleCode());
        if ($this->useSessionLocale()) {
            Session::put('locale', $this->currentLocale->code);
        }
        if ($this->useCookieLocale()) {
            Cookie::queue(Cookie::forever('locale', $this->currentLocale->code));
        } else {
            if (Cookie::get('locale') != null) {
                Cookie::forget('locale');
            }
        }

        return $localeCode;
    }

    public function setSupportedLocales($locales)
    {
        $this->supportedLocales = $locales;
    }

    public function localizeURL($url = null, $locale = null)
    {
        return $this->getLocalizedURL($locale, $url);
    }

    public function getLocalizedURL($locale = null, $url = null, $attributes = [])
    {
        if (is_null($locale)) {
            $locale = $this->getCurrentLocale();
        } elseif ($locale !== false) {
            if (!$this->isSupportedLocaleCode($locale->code)) {
                throw new UnsupportedLocaleException('Locale \''.$locale->code.'\' is not in the list of supported locales.');
            }
        }
        if (empty($attributes)) {
            $attributes = $this->extractAttributes($url);
        }
        if (empty($url)) {
            if (empty($this->routeName)) {
                $url = $this->request->fullUrl();
            } else {
                return $this->getURLFromRouteNameTranslated($locale, $this->routeName, $attributes);
            }
        } else {
            if ($locale && $translatedRoute = $this->findTranslatedRouteByUrl($url, $attributes, $this->currentLocale)
            ) {
                return $this->getURLFromRouteNameTranslated($locale, $translatedRoute, $attributes);
            }
        }
        $base_path  = $this->request->getBaseUrl();
        $parsed_url = parse_url($url);
        $urlLocale  = $this->getDefaultLocale();
        if (!$parsed_url || empty($parsed_url['path'])) {
            $path = $parsed_url['path'] = "";
        } else {
            $parsed_url['path'] = str_replace($base_path, '', '/'.ltrim($parsed_url['path'], '/'));
            $path               = $parsed_url['path'];
            foreach ($this->getSupportedLocales() as $supportedLocale) {
                $parsed_url['path'] = preg_replace('%^/?'.$supportedLocale->code.'/%', '$1', $parsed_url['path']);
                if ($parsed_url['path'] != $path) {
                    $urlLocale = $supportedLocale;
                    break;
                } else {
                    $parsed_url['path'] = preg_replace('%^/?'.$supportedLocale->code.'$%', '$1',
                        $parsed_url['path']);
                    if ($parsed_url['path'] != $path) {
                        $urlLocale = $supportedLocale;
                        break;
                    }
                }
            }
        }
        $parsed_url['path'] = ltrim($parsed_url['path'], '/');
        if ($translatedRoute = $this->findTranslatedRouteByPath($parsed_url['path'], $urlLocale)) {
            return $this->getURLFromRouteNameTranslated($locale, $translatedRoute, $attributes);
        }
        if (!empty($locale)) {
            $parsed_url['path'] = $locale->code.'/'.ltrim($parsed_url['path'], '/');
        }
        $parsed_url['path'] = ltrim(ltrim($base_path, '/').'/'.$parsed_url['path'], '/');
        //Make sure that the pass path is returned with a leading slash only if it come in with one.
        if (starts_with($path, '/') === true) {
            $parsed_url['path'] = '/'.$parsed_url['path'];
        }
        $parsed_url['path'] = rtrim($parsed_url['path'], '/');
        $url                = $this->unparseUrl($parsed_url);
        if ($this->checkUrl($url)) {
            return $url;
        }

        return $this->locale->code;
    }

    public function getURLFromRouteNameTranslated($locale, $transKeyName, $attributes = [])
    {
        if ($locale !== false && !$this->isSupportedLocaleCode($locale->code)) {
            throw new UnsupportedLocaleException('Locale \''.$locale->code.'\' is not in the list of supported locales.');
        }
        $route = "";
        if (!($locale === $this->defaultLocale)) {
            $route = '/'.$locale->code;
        }
        if ($this->translator->has($transKeyName, $locale->code)) {
            $translation = $this->translator->trans($transKeyName, [], "", $locale->code);
            $route .= "/".$translation;
            if (is_array($attributes)) {
                foreach ($attributes as $key => $value) {
                    $route = str_replace("{".$key."}", $value, $route);
                    $route = str_replace("{".$key."?}", $value, $route);
                }
            }
            // delete empty optional arguments
            $route = preg_replace('/\/{[^)]+\?}/', '', $route);
        }
        if (!empty($route)) {
            return rtrim($this->createUrlFromUri($route));
        }

        // This locale does not have any key for this route name
        return false;
    }

    public function getNonLocalizedURL($url = null)
    {
        return $this->getLocalizedURL(false, $url);
    }

    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    public function getDefaultLocaleID()
    {
        return $this->getDefaultLocale()->id;
    }

    public function getSupportedLocales()
    {
        if (!empty($this->supportedLocales)) {
            return $this->supportedLocales;
        } else {
            $this->supportedLocales = Locale::all();

            return $this->supportedLocales;
        }
    }

    public function getCurrentLocaleID()
    {
        return $this->getCurrentLocale()->id;
    }

    public function getCurrentLocaleCode()
    {
        return $this->getCurrentLocale()->code;
    }

    public function getCurrentLocaleName()
    {
        return $this->getCurrentLocale()->name;
    }

    public function getCurrentLocaleDirection()
    {
        return $this->getCurrentLocale()->direction;
    }

    public function getCurrentLocaleScript()
    {
        return $this->getCurrentLocale()->script;
    }

    public function getCurrentLocaleNativeReading()
    {
        return $this->getCurrentLocale()->native;
    }

    public function getCurrentLocaleFlag()
    {
        return $this->getCurrentLocale()->flag;
    }

    public function getCurrentLocaleCurrency()
    {
        return $this->getCurrentLocale()->currency;
    }

    /**
     * Returns supported languages language key
     * @return array    keys of supported languages
     */
    public function getCurrentLocale()
    {
        if (!empty($this->currentLocale)) {
            return $this->currentLocale;
        }
        // get session language...
        if ($this->useSessionLocale() && Session::has('locale')) {
            return $this->getLocaleFromCode(Session::get('locale'));
        } // or get cookie language...
        else {
            if ($this->useCookieLocale() && Cookie::get('locale') != null && $this->isSupportedLocaleCode(Cookie::get('locale'))) {
                return $this->getLocaleFromCode(Cookie::get('locale'));
            } // or get browser language...
            else {
                if ($this->useAcceptLanguageHeader()) {
                    return $this->negotiateLanguage();
                }
            }
        }

        // or get application default language
        return $this->getLocaleFromCode($this->configRepository->get('app.locale'));
    }

    public function getSupportedLocalesKeys()
    {
        return $this->supportedLocales->keys();
    }

    protected function getTranslatedRoutes()
    {
        return $this->translatedRoutes;
    }

    public function setRouteName($routeName)
    {
        $this->routeName = $routeName;
    }

    public function transRoute($routeName)
    {
        if (!in_array($routeName, $this->translatedRoutes)) {
            $this->translatedRoutes[] = $routeName;
        }

        return $this->translator->trans($routeName);
    }

    public function getRouteNameFromAPath($path)
    {
        $path = str_replace(url(), "", $path);
        if ($path[0] !== '/') {
            $path = '/'.$path;
        }
        $path = str_replace('/'.$this->currentLocale.'/', '', $path);
        $path = trim($path, "/");
        foreach ($this->translatedRoutes as $route) {
            if ($this->translator->trans($route) == $path) {
                return $route;
            }
        }

        return false;
    }

    protected function findTranslatedRouteByPath($path, $urlLocaleCode)
    {
        // check if this url is a translated url
        foreach ($this->translatedRoutes as $translatedRoute) {
            if ($this->translator->trans($translatedRoute, [], "", $urlLocaleCode) == $path) {
                return $translatedRoute;
            }
        }

        return false;
    }

    protected function findTranslatedRouteByUrl($url, $attributes, $locale)
    {
        // check if this url is a translated url
        foreach ($this->translatedRoutes as $translatedRoute) {
            $routeName = $this->getURLFromRouteNameTranslated($locale, $translatedRoute, $attributes);
            if ($this->getNonLocalizedURL($routeName) == $this->getNonLocalizedURL($url)) {
                return $translatedRoute;
            }
        }

        return false;
    }

    protected function checkUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    public function getConfigRepository()
    {
        return $this->configRepository;
    }

    protected function useSessionLocale()
    {
        return $this->configRepository->get('localization::useSessionLocale') || $this->configRepository->get('localization::useSessionLanguage');
    }

    protected function useCookieLocale()
    {
        return $this->configRepository->get('localization::useCookieLocale') || $this->configRepository->get('localization::useCookieLanguage');
    }

    protected function useAcceptLanguageHeader()
    {
        return $this->configRepository->get('localization::useAcceptLanguageHeader') || $this->configRepository->get('localization::useBrowserLanguage');
    }

    public function createUrlFromUri($uri)
    {
        if (empty($this->baseUrl)) {
            return URL::to($uri);
        }

        return $this->baseUrl.ltrim($uri, "/");
    }

    public function setBaseUrl($url)
    {
        if (substr($url, -1) != "/") {
            $url .= "/";
        }
        $this->baseUrl = $url;
    }

    protected function extractAttributes($url = false)
    {
        if (!empty($url)) {
            $attributes = [];
            $parse      = parse_url($url);
            $parse      = explode("/", $parse['path']);
            $url        = [];
            foreach ($parse as $segment) {
                if (!empty($segment)) {
                    $url[] = $segment;
                }
            }
            foreach ($this->router->getRoutes() as $route) {
                $path = $route->getUri();
                if (!preg_match("/{[\w]+}/", $path)) {
                    continue;
                }
                $path  = explode("/", $path);
                $i     = 0;
                $match = true;
                foreach ($path as $j => $segment) {
                    if (isset($url[ $i ])) {
                        if ($segment === $url[ $i ]) {
                            $i++;
                            continue;
                        }
                        if (preg_match("/{[\w]+}/", $segment)) {
                            // must-have parameters
                            $attribute_name                = preg_replace([
                                "/}/",
                                "/{/",
                                "/\?/"
                            ], "", $segment);
                            $attributes[ $attribute_name ] = $url[ $i ];
                            $i++;
                            continue;
                        }
                        if (preg_match("/{[\w]+\?}/", $segment)) {
                            // optional parameters
                            if (!isset($path[ $j + 1 ]) || $path[ $j + 1 ] !== $url[ $i ]) {
                                // optional parameter taken
                                $attribute_name                = preg_replace([
                                    "/}/",
                                    "/{/",
                                    "/\?/"
                                ], "", $segment);
                                $attributes[ $attribute_name ] = $url[ $i ];
                                $i++;
                                continue;
                            }
                        }
                    } else {
                        if (!preg_match("/{[\w]+\?}/", $segment)) {
                            // no optional parameters but no more $url given
                            // this route does not match the url
                            $match = false;
                            break;
                        }
                    }
                }
                if (isset($url[ $i + 1 ])) {
                    $match = false;
                }
                if ($match) {
                    return $attributes;
                }
            }
        } else {
            if (!$this->router->current()) {
                return [];
            }
            $attributes = $this->router->current()->parameters();
            $response   = \Event::fire('routes.translation', ['attributes' => $attributes]);
            if (!empty($response)) {
                $response = array_shift($response);
            }
            if (is_array($response)) {
                $attributes = array_merge($attributes, $response);
            }
        }

        return $attributes;
    }

    protected function unparseUrl($parsed_url)
    {
        if (empty($parsed_url)) {
            return "";
        }
        $url = "";
        $url .= isset($parsed_url['scheme']) ? $parsed_url['scheme'].'://' : '';
        $url .= isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $url .= isset($parsed_url['port']) ? ':'.$parsed_url['port'] : '';
        $user = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass = isset($parsed_url['pass']) ? ':'.$parsed_url['pass'] : '';
        $url .= $user.(($user || $pass) ? "$pass@" : '');
        if (!empty($url)) {
            $url .= isset($parsed_url['path']) ? '/'.ltrim($parsed_url['path'], '/') : '';
        } else {
            $url .= isset($parsed_url['path']) ? $parsed_url['path'] : '';
        }
        $url .= isset($parsed_url['query']) ? '?'.$parsed_url['query'] : '';
        $url .= isset($parsed_url['fragment']) ? '#'.$parsed_url['fragment'] : '';

        return $url;
    }

    public function negotiateLanguage()
    {
        $default   = $this->getDefaultLocale();
        $supported = $this->getSupportedLocales();
        if (!($supported->count())) {
            return $default;
        }
        if ($this->request->header('Accept-Language')) {
            $matches         = [];
            $generic_matches = [];
            foreach (explode(',', $this->request->header('Accept-Language')) as $option) {
                $option = array_map('trim', explode(';', $option));
                $l      = $option[0];
                if (isset($option[1])) {
                    $q = (float)str_replace('q=', '', $option[1]);
                } else {
                    $q = null;
                    // Assign default low weight for generic values
                    if ($l == '*/*') {
                        $q = 0.01;
                    } elseif (substr($l, -1) == '*') {
                        $q = 0.02;
                    }
                }
                // Unweighted values, get high weight by their position in the
                // list
                $q             = isset($q) ? $q : 1000 - count($matches);
                $matches[ $l ] = $q;
                //If for some reason the Accept-Language header only sends language with country
                //we should make the language without country an accepted option, with a value
                //less than it's parent.
                $l_ops = explode('-', $l);
                array_pop($l_ops);
                while (!empty($l_ops)) {
                    //The new generic option needs to be slightly less important than it's base
                    $q -= 0.001;
                    $op = implode('-', $l_ops);
                    if (empty($generic_matches[ $op ]) || $generic_matches[ $op ] > $q) {
                        $generic_matches[ $op ] = $q;
                    }
                    array_pop($l_ops);
                }
            }
            $matches = array_merge($generic_matches, $matches);
            arsort($matches, SORT_NUMERIC);
            foreach ($matches as $key => $q) {
                if ($this->isSupportedLocaleCode($key)) {
                    return $this->getLocaleFromCode($key);
                }
            }
            // If any (i.e. "*") is acceptable, return the first supported format
            if (isset($matches['*'])) {
                return $supported->shift();
            }
        }
        //		if (class_exists('Locale')) {
        //			if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        //				if ($_SERVER['HTTP_ACCEPT_LANGUAGE'] != '') {
        //					$http_accept_language = \Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']);
        //					if (in_array($http_accept_language, $supported)) {
        //						return $http_accept_language;
        //					}
        //				}
        //			}
        //		}
        if ($this->request->server('REMOTE_HOST')) {
            $remote_host = explode('.', $this->request->server('REMOTE_HOST'));
            $lang        = strtolower(end($remote_host));
            if ($this->isSupportedLocaleCode($lang)) {
                return $this->getLocaleFromCode($lang);
            }
        }

        return $default;
    }

    public function getCleanRoute($route = null)
    {
        return $this->getNonLocalizedURL($route);
    }

    public function getLocaleFromCode($code)
    {
        $supported = $this->getSupportedLocales();
        foreach ($supported as $locale) {
            if ($locale->code == $code) {
                return $locale;
            }
        }
        throw new UnsupportedLocaleException('Locale \''.$code.'\' is not in the list of supported locales.');
    }

    public function isSupportedLocaleCode($code)
    {
        $supported = $this->getSupportedLocales();
        foreach ($supported as $locale) {
            if ($locale->code == $code) {
                return true;
            }
        }

        return false;
    }
}
